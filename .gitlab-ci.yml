---
# Copyright (c) 2019, PhysK
# All rights reserved.

image: docker:stable
variables:
  BRANCH_TAG: physk/rclone-mergerfs:$CI_COMMIT_REF_NAME
  LATEST_TAG: physk/rclone-mergerfs:latest
services:
  - docker:18.09-dind

stages:
  - preflight
  - build

# Generic preflight template
.preflight: &preflight
  stage: preflight
  only:
    - master

# Generic build template
.build: &build
  stage: build
  before_script:
    - docker login -u $DOCKER_HUB_USERNAME -p $DOCKER_HUB_PASSWORD

# Shell Check
shellcheck:
  <<: *preflight
  image:
    name: koalaman/shellcheck-alpine:stable
    entrypoint: [""]
  before_script:
    - shellcheck --version
    - apk --no-cache add grep
    - |
      find . -type f -print0 | \
        xargs -0 sed -i 's:#!/usr/bin/with-contenv bash:#!/bin/bash:g'
  script:
    - |
      for file in $(grep -IRl "#\!\(/bin/\|/usr/bin/\)" "root/"); do
        if ! shellcheck $file; then
          export FAILED=1
        else
          echo "$file OK"
        fi
      done
      if [ "${FAILED}" = "1" ]; then
        exit 1
      fi

# Yaml Lint
yamllint:
  <<: *preflight
  image: sdesbure/yamllint
  before_script:
    - yamllint --version
  script:
    - yamllint .

# JSON Lint
jsonlint:
  <<: *preflight
  image: sahsu/docker-jsonlint
  before_script:
    - jsonlint --version || true
  script:
    - |
      for file in $(find . -type f -name "*.json"); do
        if ! jsonlint -q $file; then
          export FAILED=1
        else
          echo "$file OK"
        fi
      done
      if [ "${FAILED}" = "1" ]; then
        exit 1
      fi

# Markdown Lint
markdownlint:
  <<: *preflight
  image:
    name: ruby:alpine
    entrypoint: [""]
  before_script:
    - gem install mdl
    - mdl --version
  script:
    - mdl --style all --warnings .

# Build Dev Branch
build-docker-dev:
  <<: *build
  script:
    - docker build --no-cache -t $BRANCH_TAG .
    - docker push $BRANCH_TAG
  except:
    - master

# Build master Branch
build-docker-master:
  <<: *build
  script:
    - docker build --no-cache -t $LATEST_TAG .
    - docker push $LATEST_TAG
  only:
    - master
